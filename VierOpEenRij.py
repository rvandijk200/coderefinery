from tkinter import *

if __name__ == '__main__':

    canvas_width = 1000
    canvas_height = 1000

    master = Tk()

    w = Canvas(master, 
           width=canvas_width, 
           height=canvas_height)
    w.pack()

    nr_rows = 7
    nr_columns = 6

    bias_x = 50
    bias_y = 50
    circle_size = 100
    margin = 10
    border_margin = 10
    text_margin = 10

    color_player_1 = 'red'
    name_player_1 = 'player 1'

    color_player_2 = 'white'
    name_player_2 = 'player 2'

    w.create_text(
        bias_x,
        bias_y + nr_columns * (margin + circle_size) - margin + border_margin + text_margin,
        text=name_player_1 )

    w.create_text(
        bias_x,
        bias_y + nr_columns * (margin + circle_size) - margin + border_margin + 2 * text_margin,
        text=name_player_2 )

    w.create_rectangle(
        bias_x - border_margin, 
        bias_y - border_margin, 
        bias_x + nr_rows * (margin + circle_size) - margin + border_margin,
        bias_y + nr_columns * (margin + circle_size) - margin + border_margin,
        fill='blue', width=3 )

    for row in range(nr_rows):
        for column in range(nr_columns):
            x1 = bias_x + row * (margin + circle_size)
            y1 = bias_y + column * (margin + circle_size)
            x2 = x1 + circle_size
            y2 = y1 + circle_size
            w.create_oval(x1,y1,x2,y2, fill=color_player_2, width=3 )

    mainloop()

